$(function() {
  if(!document.cookie) {
    window.location.replace(window.location.origin + "/login.html");
  } else {
    var locationInput;
    var timeInput;
    var meetingNumber;

    // Get meetings from API
    sendRequest("GET", apiBaseUrl + "/user/meetings", getMeetingsData);

    // Display meeting data in UI
    function getMeetingsData(meetingData) {
      if(meetingData.length < 1) {
        $(".chat-container").hide();
        $(".meeting-information-wrapper").hide();
        $(".meeting-cards-container").append("<h1 class='meetings-header' style='padding-top:1rem;'>You currently have no meetings</h1>")
        $(".middle-content-container").append("<h1 class='meetings-header' style='padding-top:5rem;'>You can chat after you join a meeting</h1>")
        $(".right-content-container").append("<h1 class='meetings-header' style='padding-top:5rem;'>You can see more information after you join a meeting</h1>")
      } else {
        for(var i = 0; i < meetingData.length; i++) {
          $(".meeting-cards-container").append("<div id='" + i + "'class='meeting-container'> <div class='meeting-content-container'><div class='vertical-align'><h4 class='meeting-name'>" + meetingData[i].name + "</h4></div></div> <div class='meeting-content-container'><div class='vertical-align'><p id='meeting-card-location' class='meeting-description location-" + i + "'>Pending</p></div></div> <div class='meeting-content-container'><div class='meeting-location-date-container'><div class='vertical-align'><p class='meeting-description'>" + meetingData[i].meeting_date + "</p></div></div><div class='meeting-location-date-container'><div class='vertical-align'><p id='meeting-card-time' class='meeting-description time-" + i + "'>Pending</p></div></div></div></div>");

          // This avoids displaying null for location
          if(meetingData[i].location != null) {
            $(".location-" + i).text(meetingData[i].location);
          }

          // This avoids displaying null for time
          if(meetingData[i].meeting_time != null) {
            $(".time-" + i).text(meetingData[i].meeting_time.substring(11, 16));
          }
        }

        // Adds a small padding if a scrollbar exists on #meetings-scroll
        if($("#meetings-scroll").prop('scrollHeight') > $(".scroll-wrapper").height()) {
          $(".meeting-cards-container").css("cssText", "padding-right: 1rem");
        }

        // Display chat and meeting info for clicked meeting
        $(".meeting-container").click(function() {
          if($(this).attr('id') != meetingNumber) {
            meetingNumber = parseInt($(this).attr('id'));

            // Removes messages of old meeting
            $("#chat-messages-wrapper").empty();

            // Changes information to that of the new meeting
            // if statements make sure that "null" is not displayed
            if(meetingData[meetingNumber].name != null) {
              $(".meeting-name-second").text(meetingData[meetingNumber].name);
            } else {
              $(".meeting-name-second").text("Meeting " + (meetingNumber + 1));
            }
            if(meetingData[meetingNumber].location != null) {
              $("#meeting-location").text(meetingData[meetingNumber].location);
            }
            if(meetingData[meetingNumber].meeting_date != null) {
              $("#meeting-date").text(meetingData[meetingNumber].meeting_date);
            }

            // Gets voting status for meeting
            sendRequest("GET", apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/results", getMeetingVotes);

            function getMeetingVotes(meetingVotes) {
              // if both location and time are accepted
              if(meetingVotes.location_accepted == "location_accept" && meetingVotes.time_accepted == "time_accept") {
                $("#edit-meeting-heading").text("Vote for meeting");
                $(".edit-location-btn").hide();
                $(".edit-date-btn").hide();
              } else {
                $(".accept-btn").hide();
                $(".decline-btn").hide();

                // Display message if location is already accepted
                if(meetingVotes.location_accepted == "location_accept") {
                  $(".edit-location-btn").hide();
                  $("#btn-container").append("<h1 class='edit-location-btn vote-submited-heading accepted-vote'>Location Accepted</h1>")
                } else if(meetingVotes.location_accepted != "location_accept" && meetingData[meetingNumber].location != null) {
                  // if a location is already proposed but not accepted
                  // != null checks to see if a location has been proposed

                  // See if the current user has voted on the location
                  sendRequest("GET", apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/location", getLocationVoteStatus);

                  function getLocationVoteStatus(locationVoteStatus) {
                    if(locationVoteStatus.message == "true") {
                      $(".edit-location-btn").hide();
                      $("#btn-container").append("<h1 class='edit-location-btn vote-submited-heading location-vote-submited'>Vote submited</h1>")
                    } else {
                      $(".edit-location-btn").show();
                      $(".location-vote-submited").remove();
                      $(".edit-location-btn").text("Vote for location");
                      $(".edit-location-btn").attr("href", "#vote-location-modal");
                      $(".proposed-location").text(meetingData[meetingNumber].location);

                      // Location accept vote
                      $("#__button4").click(function() {
                        $.ajax({
                          url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/location/accept",
                          type: 'PUT',
                          xhrFields: {
                            withCredentials: false
                          },
                          headers: {
                            'token': message,
                            'Authorization': 'Basic ' + user_token
                          },
                          success: function(response) {
                            location.reload();
                          },
                          error: function(response) {
                            console.log(response);
                          }
                        });
                      });

                      // Location decline vote
                      $("#__button5").click(function() {
                        $.ajax({
                          url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/location/decline",
                          type: 'PUT',
                          xhrFields: {
                            withCredentials: false
                          },
                          headers: {
                            'token': message,
                            'Authorization': 'Basic ' + user_token
                          },
                          success: function(response) {
                            location.reload();
                          },
                          error: function(response) {
                            console.log(response);
                          }
                        });
                      });
                    }
                  }
                }

                // Display message if time is already accepted
                if(meetingVotes.time_accepted == "time_accept") {
                  $(".edit-date-btn").hide();
                  $("#btn-container").append("<h1 class='edit-date-btn vote-submited-heading accepted-vote'>Time Accepted</h1>")
                } else if(meetingVotes.time_accepted != "time_accept" && meetingData[meetingNumber].meeting_time != null) {
                  // if a time is already proposed but not accepted
                  // != null checks to see if a time has been proposed

                  // See if the current user has voted on the time
                  sendRequest("GET", apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/time", getTimeVoteStatus);

                  function getTimeVoteStatus(timeVoteStatus) {
                    if(timeVoteStatus.message == "true") {
                      $(".edit-date-btn").hide();
                      $("#btn-container").append("<h1 class='edit-date-btn vote-submited-heading date-vote-submited'>Vote submited</h1>")
                    } else {
                      $(".edit-date-btn").show();
                      $(".date-vote-submited").remove();
                      $(".edit-date-btn").text("Vote for time");
                      $(".edit-date-btn").attr("href", "#vote-date-modal");
                      $(".proposed-date").text(meetingData[meetingNumber].meeting_time.substring(11, 16));

                      // Time accept vote
                      $("#__button6").click(function() {
                        $.ajax({
                          url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/time/accept",
                          type: 'PUT',
                          xhrFields: {
                            withCredentials: false
                          },
                          headers: {
                            'token': message,
                            'Authorization': 'Basic ' + user_token
                          },
                          success: function(response) {
                            location.reload();
                          },
                          error: function(response) {
                            console.log(response);
                          }
                        });
                      });

                      // Time decline vote
                      $("#__button7").click(function() {
                        $.ajax({
                          url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/vote/time/decline",
                          type: 'PUT',
                          xhrFields: {
                            withCredentials: false
                          },
                          headers: {
                            'token': message,
                            'Authorization': 'Basic ' + user_token
                          },
                          success: function(response) {
                            location.reload();
                          },
                          error: function(response) {
                            console.log(response);
                          }
                        });
                      });
                    }
                  }
                }
                // Edit location
                $("#__button2").click(function() {
                  if($("#__input1-inner").val() != "") {
                    locationInput = $("#__input1-inner").val();
                  } else {
                    // KEEP OLD VALUES IF NOTHING IS INPUTED
                    locationInput = meetingData[meetingNumber].location;
                  }

                  $.ajax({
                    url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid,
                    type: 'PUT',
                    xhrFields: {
                      withCredentials: false
                    },
                    headers: {
                      'token': message,
                      'Authorization': 'Basic ' + user_token
                    },
                    data: {
                      meeting: {
                        location: locationInput
                      }
                    },
                    success: function(response) {
                      location.reload();
                    },
                    error: function(response) {
                      console.log(response);
                    }
                  });
                });

                // Submits form if "ENTER" is pressed
                $('#edit-location-container').keypress(function(e) {
                  if(e.which == 13) {
                    $("#__button2").trigger('click');
                    return false;
                  }
                });

                // Edit time
                $("#__button3").click(function() {
                  if($("#__input2-inner").val() != "") {
                    timeInput = $("#__input2-inner").val();
                  } else {
                    // KEEP OLD VALUES IF NOTHING IS INPUTED
                    timeInput = meetingData[meetingNumber].meeting_date;
                  }

                  $.ajax({
                    url: apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid,
                    type: 'PUT',
                    xhrFields: {
                      withCredentials: false
                    },
                    headers: {
                      'token': message,
                      'Authorization': 'Basic ' + user_token
                    },
                    data: {
                      meeting: {
                        meeting_time: timeInput
                      }
                    },
                    success: function(response) {
                      location.reload();
                    },
                    error: function(response) {
                      console.log(response);
                    }
                  });
                });

                // Submits form if "ENTER" is pressed
                $('#edit-date-container').keypress(function(e) {
                  if(e.which == 13) {
                    $("#__button3").trigger('click');
                    return false;
                  }
                });
              }
            }

            // Get users in selected meeting
            sendRequest("GET", apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/users", getMeetingUsers);

            // Get and display meeting status for each user in meeting
            function getMeetingStatusOfUser(meetingUsers, meetingData, meetingNumber, i) {
              sendRequest("GET", apiBaseUrl + "/users/" + meetingUsers[i].uuid + "/meetings/" + meetingData[meetingNumber].uuid + "/status", getMeetingStatus);
              // Displays meeting status for user
              function getMeetingStatus(userMeetingStatus) {
                var meetingStatus = ""; // By default we assume the user has neither declined nor accepted the meeting
                if(userMeetingStatus.message == "accept") {
                  meetingStatus = "positive";
                } else if(userMeetingStatus.message == "decline") {
                  meetingStatus = "negative";
                }
                // Appends users in meeting
                $("#bottom-data-container").append("<div class='user-information-container'><div class='picture-wrapper'><div class='user-picture-container'><img src='" + meetingUsers[i].picture + "' class='user-picture'></div><div class='meeting-status " + meetingStatus + "'></div></div><h3 class='user-name'>" + meetingUsers[i].name.split(" ")[0] + "</h3></div>");
              }
            }

            // Goes through all users in meeting and adds their status
            function getMeetingUsers(meetingUsers) {
              $("#bottom-data-container").empty();
              for(var i = 0; i < meetingUsers.length; i++) {
                getMeetingStatusOfUser(meetingUsers, meetingData, meetingNumber, i);
              }
            }
          }

          var previousMessagesCount = 0;

          (function poll() {
            setTimeout(function() {
              sendRequest("GET", apiBaseUrl + "/meetings/" + meetingData[meetingNumber].uuid + "/chat/messages", getChatInformation);

              function getChatInformation(messagesData) {
                // Append messages
                if(messagesData.length > previousMessagesCount) {
                  $("#chat-messages-wrapper").empty();
                  for(var i = 0; i < messagesData.length; i++) {
                    if(messagesData[i].sender_uuid == loggedUser.uuid) {
                      $("#chat-messages-wrapper").append("<div class='message-container logged-user-message " + i + "'><p id='user-message' style='margin-left:auto;'>" + messagesData[i].content + "</p><div class='picture-wrapper'><div class='user-picture-container chat-picture' style='margin-left:1rem;'><img src='" + loggedUser.picture + "' class='user-picture'></div></div></div>");
                    } else {
                      $("#chat-messages-wrapper").append("<div class='message-container " + i + "'><div class='picture-wrapper'><div class='user-picture-container chat-picture' style='margin-right:1rem;'><img src='" + messagesData[i].sender_picture + "' class='user-picture'></div></div><p id='user-message'>" + messagesData[i].content + "</p></div>");
                    }
                  }
                  previousMessagesCount = messagesData.length;
                  // Scrolls to the newest messages on page load
                  $("#chat-scroll").animate({
                    scrollTop: $('#chat-scroll').prop("scrollHeight")
                  }, 500);
                }
              }
              poll();
            }, 500);
          })();
        });

        // Automatically opens first meeting
        $("#0").trigger('click');

        // Opens chat for mobile
        if($(document).width() <= 768) {
          $(".meeting-container").click(function() {
            $(".middle-content-container").css("cssText", "display:block");
          });
        }

        // Submits message input form if button is pressed
        $("#__button0").click(function() {
          messageInput = $("#__input0-inner").val();
          $("#__input0-inner").val('');
          $.ajax({
            url: apiBaseUrl + "/chat/" + meetingData[meetingNumber].id + "/message",
            type: 'POST',
            xhrFields: {
              withCredentials: false
            },
            headers: {
              'token': message,
              'Authorization': 'Basic ' + user_token
            },
            data: {
              sender: loggedUser.id,
              message: messageInput
            }
          });
        });

        // Submits message input form if enter is pressed
        $('#__input0').keypress(function(e) {
          if(e.which == 13) {
            $("#__button0").trigger('click');
            return false;
          }
        });

        // Sets meeting status for user to "accept" if clicked
        $(".accept-btn").click(function() {
          sendRequest("GET", apiBaseUrl + "/meetings/respond/" + meetingData[meetingNumber].confirmation_token + "/" + loggedUser.uuid + "/accept", );
        });

        // Sets meeting status for user to "decline" if clicked
        $("#__button1").click(function() {
          sendRequest("GET", apiBaseUrl + "/meetings/respond/" + meetingData[meetingNumber].confirmation_token + "/" + loggedUser.uuid + "/decline", reloadOnDecline);

          function reloadOnDecline() {
            location.reload();
          }
        });
      }
    }
  }
});