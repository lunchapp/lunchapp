// Gets information from cookies
var splitCookie = document.cookie.split("; ");
var status = splitCookie[0].split("=")[0];
var message = splitCookie[0].split("=")[1];
var user_key = splitCookie[1].split("=")[0];
var user_token = splitCookie[1].split("=")[1];

var apiBaseUrl = "http://localhost:3000";
var loggedUser;

function sendRequest(requestType, requestUrl, callbackFunction) {
  $.ajax({
    url: requestUrl,
    type: requestType,
    xhrFields: {
      withCredentials: false
    },
    headers: {
      'token': message,
      'Authorization': 'Basic ' + user_token
    },
    success: function(response) {
      if(callbackFunction != null) {
        callbackFunction(response)
      }
    },
    error: function(response) {
      console.log(response);
    }
  });
}

sendRequest("GET", apiBaseUrl + "/user", getCurrentUser);

function getCurrentUser(response) {
  loggedUser = response;
}

$(function() {
  $(".header-logo").click(function() {
    window.location.replace(window.location.origin + "/main.html");
  });

  $(".header-dropdown-heading").click(function() {
    window.location.replace(window.location.origin + "/profile.html");
  });

  $("#company").click(function() {
    window.location.replace(window.location.origin + "/company.html");
  });

  // Handle log out
  $("#logout").click(function() {
    sendRequest("DELETE", apiBaseUrl + "/logout", handleLogOut);

    function handleLogOut(response) {
      document.cookie.split(";").forEach(function(c) {
        document.cookie = c.replace(/^ +/, "").replace(/=.*/, "=;expires=" + new Date().toUTCString() + ";path=/");
      });
      window.location.replace(window.location.origin + "/login.html");
    }
  });
});