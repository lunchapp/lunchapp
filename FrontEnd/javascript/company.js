$(function() {
  // Gets the company of the logged user
  sendRequest("GET", apiBaseUrl + "/user/company", getUserCompany);

  function getUserCompany(company) {
    // Appends company information
    $(".user-information-container").append("<h2 class='username'>" + company.name + "</h2>");
    $(".user-information-container").append("<p class='user-description'>" + company.description + "</p>");
    // Adds company's picture
    $(".user-picture").css("background-image", 'url(' + company.picture + ')');

    // Gets all users in the company
    sendRequest("GET", apiBaseUrl + "/companies/" + company.uuid + "/users", getCompanyUsers);

    // Appends each user in company
    function getCompanyUsers(companyUsers) {
      for(var i = 0; i < companyUsers.length; i++) {
        $(".company-users-container").append("<div class='company-user-information-container'><div class='user-picture-wrapper'><div class='company-user-picture-container'><img src='" + companyUsers[i].picture + "' class='user-picture'></div></div><div class='user-information-container company-user'><h3 class='company-user-name'>" + companyUsers[i].name + "</h3><h3 class='company-user-position'>" + companyUsers[i].company_position + "</h3></div></div>");
      }
    }
  }
});