$(function() {
  var nameInput = loggedUser.name;
  var positionInput = loggedUser.company_position;
  var descriptionInput = loggedUser.description;
  var pictureInput = loggedUser.picture;
  var passwordInput;

  // Appends user information
  $(".user-information-container").append("<h2 class='username'>" + loggedUser.name + "</h2>");
  $(".user-information-container").append("<h3 class='user-position'>Not specified</h3>");
  // Avoids showing 'null' in UI
  if(loggedUser.company_position != null) {
    $(".user-position").text(loggedUser.company_position);
  }
  $(".user-information-container").append("<a href='#edit-user-modal' class='open-modal-btn' rel='modal:open'>Edit</a>");
  $(".user-description-container").append("<p class='user-description'>Not specified</p>");
  // Avoids showing 'null' in UI
  if(loggedUser.description != null) {
    $(".user-description").text(loggedUser.description);
  }
  // Adds user's picture
  $(".user-picture").css("background-image", 'url(' + loggedUser.picture + ')');

  // Edit user information
  $("#__button0").click(function() {
    // SET OLD VALUES IF NOTHING IS INPUTED
    if($("#__input0-inner").val() != "") {
      nameInput = $("#__input0-inner").val();
    }
    if($("#__input1-inner").val() != "") {
      positionInput = $("#__input1-inner").val();
    }
    if($("#__input2-inner").val() != "") {
      descriptionInput = $("#__input2-inner").val();
    }
    if($("#__input3-inner").val() != "") {
      pictureInput = $("#__input3-inner").val();
    }
    if($("#__input4-inner").val() != "") {
      passwordInput = $("#__input4-inner").val();
    }

    // Send user update request
    $.ajax({
      url: apiBaseUrl + "/users/" + loggedUser.uuid,
      type: 'PUT',
      xhrFields: {
        withCredentials: false
      },
      headers: {
        'token': message,
        'Authorization': 'Basic ' + user_token
      },
      data: {
        user: {
          name: nameInput,
          company_position: positionInput,
          description: descriptionInput,
          picture: pictureInput,
          password: passwordInput,
          password_confirmation: passwordInput
        }
      },
      success: function(response) {
        location.reload();
      },
      error: function(response) {
        console.log(response);
      }
    });
  });

  // Submits form if "ENTER" is pressed
  $('#edit-user-container').keypress(function(e) {
    if(e.which == 13) {
      $("#__button0").trigger('click');
      return false;
    }
  });
});