$(function() {
  if(document.cookie) {
    window.location.replace(window.location.origin + "/main.html");
  } else {
    var apiBaseUrl = "http://localhost:3000";

    // Input field data variables
    var nameInput = "";
    var emailInput = "";
    var passwordInput = "";
    var passwordConfirmInput = "";

    $(".select-box").click(function() {
      $(".select-box").removeClass('selected');
      $(this).addClass('selected');
      if($(this).attr('id') == "login-select") {
        $("#name-form").hide(500);
        $("#confirm-password-form").hide(500);
        $("#__button0-BDI-content").text("Login!");
      } else {
        $("#name-form").show(500);
        $("#confirm-password-form").show(500);
        $("#__button0-BDI-content").text("Register!");
      }
    });

    // Selects all inputs with an id starting with '__input'
    $("input[id^='__input']").focus(function() {
      $(this).addClass('active-form');
    });

    $("input[id^='__input']").focusout(function() {
      $(this).removeClass('active-form');
    });

    // Handle Login and Register
    $("#__button0").click(function() {
      nameInput = $("#__input0-inner").val() + " " + $("#__input1-inner").val();
      emailInput = $("#__input2-inner").val();
      passwordInput = $("#__input3-inner").val();
      passwordConfirmInput = $("#__input4-inner").val();

      if($("#__button0").text() == "Register!") {
        // Register
        $.ajax({
          url: apiBaseUrl + "/users",
          type: 'POST',
          xhrFields: {
            withCredentials: false
          },
          data: {
            user: {
              name: nameInput,
              email: emailInput,
              password: passwordInput,
              password_confirmation: passwordConfirmInput
            }
          },
          success: function(response) {
            if(response.status == "error") {
              sap.ui.getCore().attachInit(function() {
                new sap.m.MessageStrip({
                  type: "Error",
                  text: response.message,
                  showIcon: true
                }).placeAt("message-container");
              });
            } else if(response.status == "success") {
              sap.ui.getCore().attachInit(function() {
                new sap.m.MessageStrip({
                  type: "Success",
                  text: response.message,
                  showIcon: true
                }).placeAt("message-container");
              });
            }
          },
          error: function(response) {
            console.log(response);
          }
        });
      } else {
        // Login
        $.ajax({
          url: apiBaseUrl + "/login",
          type: 'POST',
          xhrFields: {
            withCredentials: false
          },
          data: {
            email: emailInput,
            password: passwordInput
          },
          success: function(response) {
            if(response.status == "error") {
              sap.ui.getCore().attachInit(function() {
                new sap.m.MessageStrip({
                  type: "Error",
                  text: response.message,
                  showIcon: true
                }).placeAt("message-container");
              });
            } else {
              document.cookie = response.status + "=" + response.message + ";";
              document.cookie = "user_token=" + btoa(emailInput + ":" + passwordInput) + ";";
              window.location.replace(window.location.origin + "/main.html");
            }
          },
          error: function(response) {
            console.log(response);
          }
        });
      }
    });

    // Submits form if "ENTER" is pressed
    $('.right-content-container').keypress(function(e) {
      if(e.which == 13) {
        $("#__button0").trigger('click');
        return false;
      }
    });
  }
});