DROP DATABASE IF EXISTS LunchApp;
CREATE DATABASE LunchApp CHARSET 'utf8';
USE LunchApp;

CREATE TABLE Company(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    uuid VARCHAR(255) NOT NULL,
    name VARCHAR(255) NOT NULL,
    email VARCHAR(255) NOT NULL,
    description VARCHAR(255)
);

CREATE TABLE Token(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    uuid VARCHAR(255) NOT NULL
);

CREATE TABLE User(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	email VARCHAR(255) NOT NULL,
    name VARCHAR(255),
    picture VARCHAR(255),
    company_position VARCHAR(255),
    company_id INTEGER NOT NULL,
    password VARCHAR(255),
    password_confirmation_token INTEGER,
    uuid VARCHAR(255) NOT NULL,
    description VARCHAR(255),
    created_at DATETIME,
    
    FOREIGN KEY (Company_id) REFERENCES Company(Id),
    FOREIGN KEY (password_confirmation_token) REFERENCES Token(id)
    
);

CREATE TABLE UsersMet(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    first_user_id INTEGER NOT NULL,
    secound_user_id INTEGER NOT NULL,
    
    FOREIGN KEY (first_user_id) REFERENCES User(id),
	FOREIGN KEY (secound_user_id) REFERENCES User(id)
);


CREATE TABLE Meeting(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
	name VARCHAR(255),
    created_at DATETIME NOT NULL,
    location VARCHAR(255),
    meeting_time DATETIME,
    uuid VARCHAR(255) NOT NULL,
	description VARCHAR(255)
);

CREATE TABLE STATUS(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    
    name VARCHAR(15) DEFAULT 'Pending'
);

CREATE TABLE UserMeetingStatusConnection(
	id INTEGER AUTO_INCREMENT NOT NULL PRIMARY KEY,
    
    
	user_id INTEGER NOT NULL,
    meeting_id INTEGER NOT NULL,
    status_id INTEGER NOT NULL,
    
    created_at DATETIME NOT NULL,

	FOREIGN KEY (status_id) REFERENCES Status(id),
    FOREIGN KEY (user_id) REFERENCES User(id),
    FOREIGN KEY (meeting_id) REFERENCES Meeting(id)
);

CREATE TABLE Chat(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    created_at DATETIME,
    uuid VARCHAR(255) NOT NULL,
    user_id INTEGER NOT NULL
);

CREATE TABLE Message(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    user_id INTEGER NOT NULL,
    chat_id INTEGER NOT NULL,
    created_at DATETIME,
    updated_at DATETIME,
    
    FOREIGN KEY (chat_id) REFERENCES Chat(id),
    content VARCHAR(255)
);

CREATE TABLE UserChatConnection(
	user_id INTEGER NOT NULL,
    chat_id INTEGER NOT NULL,
    
    PRIMARY KEY(user_id, chat_id),
    FOREIGN KEY (user_id) REFERENCES User(id),
    FOREIGN KEY (chat_id) REFERENCES Chat(id)
);


CREATE TABLE MeetingTokenConnection(
	id INTEGER NOT NULL AUTO_INCREMENT PRIMARY KEY,
    meeting_id INTEGER NOT NULL,
    token_id INTEGER NOT NULL,
    
    FOREIGN KEY(meeting_id) REFERENCES meeting(id),
    FOREIGN KEY(token_id) REFERENCES token(id)
);

