require 'date'

class Meeting < ApplicationRecord
  has_many :user_meeting_connections, dependent: :destroy
  has_many :users, :through => :user_meeting_connections
  has_many :meeting_votes
  after_create :generate_votes_table

  validates_presence_of :ammount_of_people

  def add_users_to_met
    self.users.each do |u|
      self.users.each do |s|
        if u.id != s.id &&
          MetUser.where(user_id: s.id, second_user_id: u.id).first == nil &&
           MetUser.where(user_id: u.id, second_user_id: s.id).first == nil
          MetUser.create(user_id: u.id, second_user_id: s.id)
        end
      end
    end
  end

  def generate_votes_table
    MeetingVote.create(meeting_id: self.id)
  end
  def send_confirmation_mail(user)
    ConfirmationMailer.confirm_meeting(self,user).deliver_now
  end
end
