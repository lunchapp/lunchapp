class Message < ApplicationRecord
  belongs_to :user
  belongs_to :chat
  before_create :set_sender_defaults

  def set_sender_defaults
    user = User.find_by(id: self.user_id)

    self.sender_uuid = user.uuid
    self.sender_picture = user.picture
  end

  validates_presence_of :content
end
