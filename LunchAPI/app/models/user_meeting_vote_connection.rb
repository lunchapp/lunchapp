class UserMeetingVoteConnection < ApplicationRecord
  belongs_to :user
  belongs_to :meeting_vote

  enum vote_type: [:location, :time]

  validates_presence_of :vote_type, :user_id, :meeting_vote_id
end
