require 'active_support/concern'

module TokenAndUuidConcern
  extend ActiveSupport::Concern

  def generate_defaults
    if self.respond_to? :uuid
      if self.uuid == nil && self.uuid != "123123"
        loop do
          self.uuid = SecureRandom.uuid
          break unless UsedTokenAndUuid.where(content:self.uuid).exists?
        end
        UsedTokenAndUuid.create(content: self.uuid)
      end
    end

    if self.respond_to? :confirmation_token
      self.confirmation_token = generate_token
    end

  end

  def generate_token
    loop do
     @token = SecureRandom.hex(64)
     break unless UsedTokenAndUuid.where(content: @token).exists?
    end
    UsedTokenAndUuid.create(content: @token)
    return @token
  end

end
