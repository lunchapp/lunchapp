require 'bcrypt'

class User < ApplicationRecord
  include ResponseConcern

  before_save {self.email = email.downcase}

  validate do
    @company = Company.find_by(email_ending: email.split("@")[1])
    if @company == nil
      errors.add(:base, "There is no company with such corporate email.")
    else
      self.company_id = @company.id
    end
  end


  validates :password, presence: true, confirmation: true, length: { minimum: 6 }
  validates :password_confirmation, presence: true, confirmation: true, length: { minimum: 6 }
  validates :email, presence: true, format: {with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i}, uniqueness: true
  validates :name, presence: true

  def has_met(user)
    return self.met_users.find_by(second_user_id: user.id) != nil ||
     user.met_users.find_by(second_user_id: self.id) != nil
  end

  def send_confirmation_mail
    ConfirmationMailer.confirm_user(self).deliver_now
    puts variable("confirmation_link",
      "http://localhost:3000/users/confirm/" + self.confirmation_token)
  end

  has_many :user_meeting_connections, dependent: :destroy
  has_many :meetings, :through => :user_meeting_connections



  has_many :messages, dependent: :destroy
  has_many :chats, :through => :messages

  has_many :met_users, dependent: :destroy

  has_secure_password

end
