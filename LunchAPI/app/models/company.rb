class Company < ApplicationRecord
  before_create :set_default_values

  has_many :users

  validates :email, presence: true, format: {with: /\A[\w+\-.]+@[a-z\d\-]+(\.[a-z\d\-]+)*\.[a-z]+\z/i}, uniqueness: true

  def set_default_values
    self.email = email.downcase
    self.email_ending = self.email.split("@")[1]
  end
end
