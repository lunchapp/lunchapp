class MeetingVote < ApplicationRecord
  belongs_to :meeting
  before_save :check_votes

  enum time_accepted: [:time_pending,:time_accept]
  enum location_accepted: [:location_pending, :location_accept]

  def check_votes
    meeting = Meeting.find_by(id: self.meeting_id)
    user_meeting_vote_connection = UserMeetingVoteConnection.where(meeting_vote_id: self.id).first

    if self.positive_location_votes > meeting.ammount_of_people/2
      self.location_accepted = "location_accept"
    end

    if self.positive_time_votes > meeting.ammount_of_people/2
      self.time_accepted = "time_accept"
    end

    if self.negative_location_votes >= meeting.ammount_of_people/2
      self.positive_location_votes = 0
      self.negative_location_votes = 0
      self.location_accepted = "location_pending"
      user_meeting_vote_connection.where(vote_type: "location").destroy_all
      meeting.update_attribute(location, nil)
    end

    if self.negative_time_votes >= meeting.ammount_of_people/2
      self.positive_time_votes = 0
      self.negative_time_votes = 0
      self.time_accepted = "time_pending"
      user_meeting_vote_connection.where(vote_type: "time").destroy_all
      meeting.update_attribute(time, nil)
    end
  end

end
