class UserMeetingConnection < ApplicationRecord
  belongs_to :user
  belongs_to :meeting

  enum status: [:accept, :decline, :pending]
end
