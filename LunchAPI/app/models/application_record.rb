class ApplicationRecord < ActiveRecord::Base
    include TokenAndUuidConcern
    
  before_create :generate_defaults
  self.abstract_class = true
end
