class ApplicationMailer < ActionMailer::Base
  default from: 'ale.laskintues19@gmail.com'
  layout 'mailer'
end
