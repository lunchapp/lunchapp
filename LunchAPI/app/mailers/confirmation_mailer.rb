class ConfirmationMailer < ApplicationMailer
  def confirm_user(user)
    @user = user
    mail to: @user.email
  end

  def confirm_meeting(meeting,user)
    @meeting = Meeting.find_by(id: meeting)
    @user = User.find_by(id: user)
    mail to: @user.email
  end
end
