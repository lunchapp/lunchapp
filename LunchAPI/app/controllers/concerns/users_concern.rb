require 'active_support/concern'

module UsersConcern
  include ResponseConcern
    extend ActiveSupport::Concern

  def log_in(user)
    if user.session_token != "123123"
      user.update_column(:session_token,generate_token)
    end
  end

  def confirmed_account?
    if get_current_user == nil
      render json: error("There is no user with such session token.")
      return false
    else
      return get_current_user.confirmed
    end
  end

  def get_current_user
     User.find_by(session_token: request.headers["token"])
  end

  def check_role_moderator
    if check_role_admin
      return true
    end
    if check_role("moderator")
      return true
    else
      render json: error("Access denied.")
      return false
    end
  end

  def check_role_admin
      if check_role("admin")
        return true
      else
         render json: error("Access denied.")
         return false
      end
  end

  def check_role(role)
    if get_current_user == nil
      render json: error("There is no user with such session token.")
      return false
    end
    if get_current_user.role == role
        return true
    else
      return false
    end
  end

  def is_current_user?(uuid)
    return uuid == get_current_user.uuid
  end

  def front_end_url
    "http://127.0.0.1:8887"
  end

  def authenticate_user
    if get_current_user == nil
      render json: error("There is no user with such session token.")
      return false
    end
     if !confirmed_account?
       render json: error("You have not confirmed your account yet.")
       return false
     end
    user = get_current_user
    authenticate_or_request_with_http_basic('BasicAuth') do |email, password|
      email.downcase == user.email && user.authenticate(password)
      return true
    end
  end
end
