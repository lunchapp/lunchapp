require 'active_support/concern'
module ResponseConcern
  extend ActiveSupport::Concern
  #shared module which helps forming a universal response
  def variable(status, message)
    return Response.new(status,message).to_json
  end


  #error and success are separate because they are most used in the API
  def error(message)
    return variable("error",message)
  end

  def success(message)
    return variable("success",message)
  end
end

class Response
  attr_accessor :status, :message

  def initialize(status, message)
    self.status = status
    self.message = message
  end
end
