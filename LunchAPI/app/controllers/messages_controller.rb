class MessagesController < ApplicationController

  def create
    message = Message.create(user_id: params[:sender], 
    chat_id: params[:chat_id], content: params[:message])

    if message.save
      render json: success("Message successfully sent.")
    else
      render json: message.errors
    end
  end
end
