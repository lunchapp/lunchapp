class SessionsController < ApplicationController
  skip_before_action :authenticate_user, only: :create

  def create
    if user = User.find_by(email: params[:email].downcase)
      if user.confirmed == false
        render json: error("Account not confirmed.")
        return
      end
      if user.authenticate(params[:password])
        if user.session_token != "123123"
          user.update_column(:session_token, generate_token)
        end
        render json: variable("session_token", user.session_token)
      else
        render json: error("Wrong password")
      end
    else
      render json: error("There is no user with such email")
    end
  end

  def destroy
    if request.headers["token"] == "123123"
      render json: success("You have successfully logged out.")
      return
    end
    token = UsedTokenAndUuid.find_by(content: request.headers["token"])

    if token != nil
      token.destroy
    else
      render json: error("Wrong token")
      return
    end

    if user = User.find_by(session_token: request.headers["token"])
      user.update_column(:session_token,nil)
    else
      render json: error("No user with such session token found.")
      return
    end

    render json: success("You have successfully logged out.")
    return
  end

end
