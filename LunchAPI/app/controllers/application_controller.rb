class ApplicationController < ActionController::API
  include ActionController::HttpAuthentication::Basic::ControllerMethods
  include TokenAndUuidConcern
  include UsersConcern
  include ResponseConcern

  before_action :authenticate_user, except: [:check_existance,:set_access_headers]

private

  def check_existance
    table_name = self.class.to_s.split("Controller")[0].singularize.titleize.constantize
    if table_name.find_by(uuid: params[:uuid]) == nil
      render json: error("There is no " + table_name.to_s.downcase + " with such uuid.")
      return false
    end
    return true
  end

  def get_record_path(record)
    @record_name = record.class.to_s.pluralize.downcase
    '/' + @record_name + '/' + record.uuid
  end

  def site_url
    return 'http://127.0.0.1:8887/'
  end

end
