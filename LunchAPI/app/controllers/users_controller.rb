class UsersController < ApplicationController
  skip_before_action :authenticate_user, only: [:create, :confirm]
  before_action :check_existance, only: [:get_user,:get_user_meetings, :get_user_chats, :update, :destroy]
  before_action :check_role_moderator, only: [:index]
  before_action :confirmed_account?, except: [:create, :confirm, :user_params]

  def show
      user = User.select(serializable_params).find_by(uuid: params[:uuid])
      render json: user
  end

  def get_user
    return User.find_by(uuid: params[:uuid])
  end

  def index
      render json: User.select(serializable_params).all.to_json
  end

  def create
    user = User.new(user_params)
    if user.name == nil or user.name == " "
      render json: error("Name cannot be blank.")
      return
    elsif user.email == nil
      render json: error("Email cannot be blank.")
      return
    elsif Company.find_by(email_ending: user.email.split("@")[1]) == nil
      render json: error("There is no company with such email ending.")
      return
    elsif user.password != user.password_confirmation
      render json: error("Password and password confirmation mismatch.")
      return
    elsif user.password.length < 6
        render json: error("Password can be a minimum of 6 symbols.")
        return
    end
    if user.save
      user.send_confirmation_mail
      render json: success("You should now go to your email to confirm your account.")
    else
      render json: error("An unknown error occured.")
    end
  end

  def get_current
    render json: get_current_user.to_json
  end

  def get_current_user_meetings
    render json: get_current_user.meetings.to_json
  end

  def confirm
      user = User.find_by(confirmation_token: params[:confirmation_token])

      if user != nil
        if user.confirmed == true
          render json: error("User has already been confirmed.")
          return
        else
          user.update_column(:confirmed,true)
          # render json: success("You have successfully confirmed your account.")
          redirect_to front_end_url + '/login.html'
        end
      else
        render json: error("There is no user with such confirmation token.")
      end
  end

  def get_user_meetings
      render json: get_user.meetings.to_json
  end

  def get_user_company
    render json: Company.find_by(id: get_user.company_id).to_json
  end

  def get_current_user_company
    render json: Company.find_by(id: get_current_user.company_id)
  end

  def get_user_chats
      render json: get_user.chats.to_json
  end

  def get_meeting_status
    user = User.find_by(uuid: params[:user_uuid])
    meeting = Meeting.find_by(uuid: params[:meeting_uuid])
    render json: variable("status",UserMeetingConnection.find_by(user_id: user.id,
    meeting_id: meeting.id).status)
  end

  def get_met_users
      render json: User.where(id: MetUser.where(user_id: get_user.id).
      pluck(:second_user_id) + MetUser.where(second_user_id: get_user.id).
      pluck(:user_id)).to_json
  end

  def update
    user = get_user
   
    if user.update_attributes(user_params)
         render json: user.to_json
    else
         render json: user.errors
    end
  end

  def destroy
      get_user.destroy
      render json: success("You have successfully deleted your account.")
  end
  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                           :password_confirmation,:company_position, :picture, :description)
    end

    def serializable_params
      "id, uuid, name, email, company_position, picture, confirmation_token,
      company_id, description, confirmed, session_token, role"
    end
end
