class CompaniesController < ApplicationController
  before_action :check_existance, only: [:show,:update,:destroy,:company_users]
  before_action :check_role_admin, only: [:create, :destroy,:index]
  before_action :check_role_moderator, only: [:update]

  def show
    render json: Company.find_by(uuid: params[:uuid]).to_json
  end

  def index
    render json: Company.all.to_json
  end

  def create
    company = Company.new(company_params)
    if company.save
      render json: success(company.name + " registered successfully.")
    else
      render json: company.errors
    end
  end

  def update
    company = Company.find_by(uuid: params[:uuid])
    company.update_attributes(company_params)
    render json: company.to_json
  end

  def destroy
    Company.find_by(uuid: params[:uuid]).delete
    render json: success("You have successfully destroied your company.")
  end

  def company_users
    render json: Company.find_by(uuid: params[:uuid]).users
  end

  private
    def company_params
      params.require(:company).permit(:name, :email, :picture, :description)
    end
end
