class MeetingsController < ApplicationController
  before_action :check_existance, only: [:get_meeting, :vote_for_location, :vote_for_time, :show, :destroy, :get_users_in_meeting, :voted_for_time?, :voted_for_location?]
  before_action :confirmed_account?, except: [:create, :meeting_params]
  before_action :authenticate_user, except: [:set_status]
  before_action :check_role_admin, only: [:index]

  def create
  end

  def get_meeting
      return Meeting.find_by(uuid: params[:uuid])
  end

  def index
    render json: Meeting.all.to_json
  end

  def show
    render json:  get_meeting.to_json
  end

  def update
    meeting = get_meeting
    if UserMeetingConnection.where(meeting_id: meeting.id, user_id: get_current_user.id).count == 0
      render json: error("You cannot change other peoples meetings.")
      return
    end
    meeting.update_attributes(meeting_params)
    render json: meeting.to_json
  end

  def nil?(check)
    if check == nil
      return true
    else
      return false
    end
  end

  def vote_for_location
    vote = params[:vote]
    meeting = get_meeting
    current_user = get_current_user
    meeting_vote = MeetingVote.find_by(meeting_id: meeting.id)

    if meeting_vote == nil
      render json: error("Meeting vote not created")
      return
    end

    if UserMeetingVoteConnection.where(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "location").count != 0
          render json: error("You have already voted for location")
          return
    end

    if vote == "accept"
      UserMeetingVoteConnection.create(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "location")
      meeting_vote.update_attribute(:positive_location_votes, meeting_vote.positive_location_votes + 1)
      render json: success("Positive location vote successful")
      return
    elsif vote == "decline"
      UserMeetingVoteConnection.create(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "location")
      meeting_vote.update_attribute(:negative_location_votes, meeting_vote.negative_location_votes + 1)
      render json: success("Negative location vote successful")
      return
    else
      render json: error("Invalid vote option")
    end

  end

  def vote_for_time
    vote = params[:vote]
    meeting = get_meeting
    current_user = get_current_user
    meeting_vote = MeetingVote.find_by(meeting_id: meeting.id)

    if UserMeetingVoteConnection.where(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "time").count != 0
          render json: error("You have already voted for time")
          return
    end

    if vote == "accept"
      UserMeetingVoteConnection.create(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "time")
      meeting_vote.update_attribute(:positive_time_votes, meeting_vote.positive_time_votes + 1)
      render json: success("Positive time vote successful")
      return
    elsif vote == "decline"
      UserMeetingVoteConnection.create(user_id: current_user.id, meeting_vote_id: meeting_vote.id, vote_type: "time")
      meeting_vote.update_attribute(:negative_time_votes, meeting_vote.negative_time_votes + 1)
      render json: success("Negative time vote successful")
      return
    else
      render json: error("Invalid vote option")
    end

  end

  def voted_for_time?
    render json: variable("time",(UserMeetingVoteConnection.where(user_id: get_current_user.id, meeting_vote_id: MeetingVote.find_by(meeting_id: get_meeting.id),vote_type: "time").count != 0).to_s)
  end

  def voted_for_location?
    render json: variable("location",(UserMeetingVoteConnection.where(user_id: get_current_user.id, meeting_vote_id: MeetingVote.find_by(meeting_id: get_meeting.id),vote_type: "location").count != 0).to_s)
  end

  def get_vote_results
    render json: MeetingVote.select(:id,:time_accepted,:location_accepted).find_by(meeting_id: Meeting.find_by(uuid: params[:uuid]).id).to_json
  end


  def set_status
    status = params[:status]

    if status != "accept" && status != "decline"
      render json: error("Invalid status")
      return
    end


    user = User.find_by(uuid: params[:user_uuid])
    meeting = Meeting.find_by(confirmation_token: params[:confirmation_token])

    if nil?(user) || nil?(meeting)
      render json: error("There is no such user or meeting")
      return
    end

    if (user_meeting_connection =
      UserMeetingConnection.find_by(user_id: user.id,
         meeting_id: meeting.id)) == nil
      render json: error("The user is not in this meeting")
      return
    end

    if status == "decline"
      user_meeting_connection.delete

      if meeting.users.count < 2
        meeting.delete
      end
      return
    end

    user_meeting_connection.update_attribute(:status, status)
    redirect_to front_end_url + "/main.html"
  end

  def destroy
    get_meeting.delete
    render json: success("You have successfully deleted this meeting.")
  end

  def get_users_in_meeting
    if get_current_user.meetings.find_by(uuid: params[:uuid]) == nil
      render json: error("You cannot see other users meetings.")
      return
    end
    render json: get_meeting.users.to_json
  end

  private

  def meeting_params
    params.require(:meeting).permit(:meeting_time, :name, :location, :description)
  end
end
