class ChatsController < ApplicationController

  def get_chat_by_meeting_uuid
    meeting = Meeting.find_by(uuid: params[:uuid])
    if meeting == nil
      render json: error("There is no meeting with such uuid.")
    end
    if meeting.users.where(uuid: get_current_user.uuid)
      chat = Chat.find_by(meeting_id: meeting.id)
      if chat == nil
        render json: error("There is no chat for this meeting.")
      end
      return chat
    else
      return error("You cannot see other users chats.")
    end
  end

  def get_meeting_chat
    render json: get_chat_by_meeting_uuid.to_json
  end

  def get_messages_of_meeting_chat
      render json: get_chat_by_meeting_uuid.messages.all.order(created_at: :asc).to_json
  end
end
