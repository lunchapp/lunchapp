class CreateUsedTokenAndUuids < ActiveRecord::Migration[5.1]
  def change
    create_table :used_token_and_uuids do |t|
      t.string :content

      t.timestamps
    end
  end
end
