class CreateUsers < ActiveRecord::Migration[5.1]
  def change
    create_table :users do |t|
      t.string :name
      t.string :password
      t.string :password_confirmation
      t.string :password_digest
      t.string :email
      t.string :picture
      t.string :session_token
      t.string :company_position
      t.integer :company_id
      t.string :confirmation_token
      t.boolean :confirmed, default: false
      t.string :role, default: "user"
      t.string :uuid
      t.string :description

      t.index :uuid, unique: true
      t.index :email, unique: true
      t.timestamps
    end
  end
end
