class CreateMessages < ActiveRecord::Migration[5.1]
  def change
    create_table :messages do |t|
      t.integer :user_id
      t.integer :chat_id
      t.string :sender_uuid
      t.string :sender_picture
      t.text :content

      t.timestamps
    end
  end
end
