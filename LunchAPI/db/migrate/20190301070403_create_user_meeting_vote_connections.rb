class CreateUserMeetingVoteConnections < ActiveRecord::Migration[5.1]
  def change
    create_table :user_meeting_vote_connections do |t|
      t.integer :user_id
      t.integer :meeting_vote_id
      t.integer :vote_type

      t.timestamps
    end
  end
end
