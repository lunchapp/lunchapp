class CreateChats < ActiveRecord::Migration[5.1]
  def change
    create_table :chats do |t|
      t.string :uuid
      t.integer :meeting_id
      
      t.index :uuid, unique: true
      t.timestamps
    end
  end
end
