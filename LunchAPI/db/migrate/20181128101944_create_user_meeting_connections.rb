class CreateUserMeetingConnections < ActiveRecord::Migration[5.1]
  def change
    create_table :user_meeting_connections do |t|
      t.integer :user_id
      t.integer :status, default: "pending"
      t.integer :meeting_id

      t.timestamps
    end
  end
end
