class CreateCompanies < ActiveRecord::Migration[5.1]
  def change
    create_table :companies do |t|
      t.string :uuid
      t.string :name
      t.string :email
      t.string :email_ending
      t.string :description
      t.string :picture

      t.index :uuid, unique: true
      t.index :email, unique: true
      t.timestamps
    end
  end
end
