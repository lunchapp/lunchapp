class CreateMeetings < ActiveRecord::Migration[5.1]
  def change
    create_table :meetings do |t|
      t.string :confirmation_token
      t.date :meeting_date
      t.time :meeting_time, default: nil
      t.string :name
      t.string :location, default: nil
      t.string :uuid
      t.string :description
      t.integer :ammount_of_people

      t.index :uuid, unique: true
      t.timestamps
    end
  end
end
