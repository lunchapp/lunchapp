class CreateMeetingVotes < ActiveRecord::Migration[5.1]
  def change
    create_table :meeting_votes do |t|
      t.integer :meeting_id
      t.integer :positive_location_votes, default: 0
      t.integer :positive_time_votes, default: 0
      t.integer :negative_location_votes, default: 0
      t.integer :negative_time_votes, default: 0
      t.integer :location_accepted, default: "location_pending"
      t.integer :time_accepted, default: "time_pending"

      t.timestamps
    end
  end
end
