Company.create(name: "TUES", email: "ale@elsys-bg.org", description: "A short company description",
  picture: "https://elsys.github.io/c-programming/assets/images/logo.png")

User.create(uuid:123123, name: "Test Account", email: "test@elsys-bg.org",company_position: "CEO",
   password: "123123", password_confirmation: "123123",
   confirmed: true, role: "admin", description: "A short description...", session_token:"123123",
  picture: "https://3c1703fe8d.site.internapcdn.net/newman/gfx/news/2018/europeslostf.jpg")

User.create(name: "Alex Laskin", email: "alex_l1@elsys-bg.org", company_position: "Developer", password: "123123",
  password_confirmation: "123123",
  confirmed: true, role: "admin",picture: "https://greenglobaltravel.com/wp-content/uploads/Guanaco_Patagonia-edited.jpg",
  description: "Not so long description which is here only for testing purposes of this admin account")

User.create(name: "Martin Todorov", email: "alex_l2@elsys-bg.org",company_position: "Developer", password: "123123",
  password_confirmation: "123123",
  confirmed: true, role: "moderator",picture: "https://s3.reutersmedia.net/resources/r/?d=20190502&i=RCV006O97&w=&r=RCV006O97&t=2",
  description: "Not so long description which is here only for testing purposes of this moderator account")

User.create(name: "John Doe", email: "alex_l3@elsys-bg.org", company_position: "Director", password: "123123",password_confirmation: "123123",
  confirmed: true, role: "user", picture: "https://www.worldatlas.com/r/w728-h425-c728x425/upload/66/14/d8/kangchenjunga.jpg", description: "Not so long description which is here only for testing purposes of this normal account")

User.create(name: "Jane Doe", email: "alex_l4@elsys-bg.org", company_position: "Marketing", password: "123123",password_confirmation: "123123",
  confirmed: true, role: "user", picture: "https://img-aws.ehowcdn.com/877x500p/s3-us-west-1.amazonaws.com/contentlab.studiod/getty/f24b4a7bf9f24d1ba5f899339e6949f3", description: "Not so long description which is here only for testing purposes of this normal account")

User.create(name: "Lorem Ipsum", email: "alex_l5@elsys-bg.org", company_position: "Manager", password: "123123",password_confirmation: "123123",
  confirmed: true, role: "admin", picture: "http://www.hotel-r.net/im/hotel/gb/hillside-0.jpg", description: "Not so long description which is here only for testing purposes of this admin account")

User.create(name: "Jermaine Cole", email: "alex_l6@elsys-bg.org", company_position: "PR", password: "123123",password_confirmation: "123123",
  confirmed: true, role: "moderator", picture: "https://www.adorama.com/alc/wp-content/uploads/2018/08/san-juans-feature-825x465.jpg", description: "Not so long description which is here only for testing purposes of this moderator account")


Meeting.create(meeting_date: Date.today + 1, meeting_time: Time.zone.now, name: "Meeting 1",
location: "TUES", description: "Meeting 1's description", ammount_of_people: 4)
UserMeetingConnection.create(user_id:1,meeting_id:1,status: "pending")
UserMeetingConnection.create(user_id:2,meeting_id:1,status: "accept")
UserMeetingConnection.create(user_id:3,meeting_id:1,status: "decline")
UserMeetingConnection.create(user_id:4,meeting_id:1,status: "accept")
Chat.create(meeting_id: 1)
Message.create(chat_id:1 , user_id: 1, content: "Test message one")
Message.create(chat_id:1 , user_id: 2, content: "Test message two")
Message.create(chat_id:1 , user_id: 3, content: "Test message three")
Message.create(chat_id:1 , user_id: 4, content: "Test message four")
Message.create(chat_id:1 , user_id: 1, content: "Test message five")
Message.create(chat_id:1 , user_id: 2, content: "Test message six")
Message.create(chat_id:1 , user_id: 3, content: "Test message seven")


Meeting.create(meeting_date: Date.today, meeting_time: Time.zone.now, name: "Meeting 2",
   location: "Naroden", description: "Meeting 2's description", ammount_of_people: 4)

UserMeetingConnection.create(user_id:1,meeting_id:2,status: "pending")
UserMeetingConnection.create(user_id:2,meeting_id:2,status: "accept")
UserMeetingConnection.create(user_id:3,meeting_id:2,status: "accept")
UserMeetingConnection.create(user_id:4,meeting_id:2,status: "accept")
Chat.create(meeting_id: 2)
Message.create(chat_id:2 , user_id: 1, content: "Test one")
Message.create(chat_id:2 , user_id: 2, content: "Test two")
Message.create(chat_id:2 , user_id: 3, content: "Test three")
Message.create(chat_id:2 , user_id: 4, content: "Test four")
Message.create(chat_id:2 , user_id: 1, content: "Test five")
Message.create(chat_id:2 , user_id: 2, content: "Test six")
Message.create(chat_id:2 , user_id: 3, content: "Test seven")

Meeting.create(meeting_date: Date.today - 1, meeting_time: Time.zone.now, name: "Meeting 3",
   location: "Buffet", description: "Meeting 3's description", ammount_of_people: 4)
UserMeetingConnection.create(user_id:1,meeting_id:3,status: "pending")
UserMeetingConnection.create(user_id:2,meeting_id:3,status: "accept")
UserMeetingConnection.create(user_id:3,meeting_id:3,status: "decline")
UserMeetingConnection.create(user_id:4,meeting_id:3,status: "decline")
Chat.create(meeting_id: 3)
Message.create(chat_id:3 , user_id: 1, content: "Message one")
Message.create(chat_id:3 , user_id: 2, content: "Message two")
Message.create(chat_id:3 , user_id: 3, content: "Message three")
Message.create(chat_id:3 , user_id: 4, content: "Message four")

Meeting.create(meeting_date: Date.today - 2, meeting_time: Time.zone.now, name: "Meeting 4",
   location: "Metro", description: "Meeting 4's description", ammount_of_people: 4)
UserMeetingConnection.create(user_id:1,meeting_id:4,status: "pending")
UserMeetingConnection.create(user_id:2,meeting_id:4,status: "accept")
UserMeetingConnection.create(user_id:3,meeting_id:4,status: "accept")
UserMeetingConnection.create(user_id:4,meeting_id:4,status: "accept")
Chat.create(meeting_id: 4)
Message.create(chat_id:4 , user_id: 1, content: "Message 1")
Message.create(chat_id:4 , user_id: 2, content: "Message 2")
Message.create(chat_id:4 , user_id: 1, content: "Message 3")
Message.create(chat_id:4 , user_id: 3, content: "Message 4")
