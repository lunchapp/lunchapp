# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20190301070403) do

  create_table "chats", force: :cascade do |t|
    t.string "uuid"
    t.integer "meeting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uuid"], name: "index_chats_on_uuid", unique: true
  end

  create_table "companies", force: :cascade do |t|
    t.string "uuid"
    t.string "name"
    t.string "email"
    t.string "email_ending"
    t.string "description"
    t.string "picture"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_companies_on_email", unique: true
    t.index ["uuid"], name: "index_companies_on_uuid", unique: true
  end

  create_table "meeting_votes", force: :cascade do |t|
    t.integer "meeting_id"
    t.integer "positive_location_votes", default: 0
    t.integer "positive_time_votes", default: 0
    t.integer "negative_location_votes", default: 0
    t.integer "negative_time_votes", default: 0
    t.integer "location_accepted", default: 0
    t.integer "time_accepted", default: 0
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "meetings", force: :cascade do |t|
    t.string "confirmation_token"
    t.date "meeting_date"
    t.time "meeting_time"
    t.string "name"
    t.string "location"
    t.string "uuid"
    t.string "description"
    t.integer "ammount_of_people"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["uuid"], name: "index_meetings_on_uuid", unique: true
  end

  create_table "messages", force: :cascade do |t|
    t.integer "user_id"
    t.integer "chat_id"
    t.string "sender_uuid"
    t.string "sender_picture"
    t.text "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "met_users", force: :cascade do |t|
    t.integer "user_id"
    t.integer "second_user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "used_token_and_uuids", force: :cascade do |t|
    t.string "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_meeting_connections", force: :cascade do |t|
    t.integer "user_id"
    t.integer "status", default: 0
    t.integer "meeting_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "user_meeting_vote_connections", force: :cascade do |t|
    t.integer "user_id"
    t.integer "meeting_vote_id"
    t.integer "vote_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "name"
    t.string "password"
    t.string "password_confirmation"
    t.string "password_digest"
    t.string "email"
    t.string "picture"
    t.string "session_token"
    t.string "company_position"
    t.integer "company_id"
    t.string "confirmation_token"
    t.boolean "confirmed", default: false
    t.string "role", default: "user"
    t.string "uuid"
    t.string "description"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["uuid"], name: "index_users_on_uuid", unique: true
  end

end
