require 'date'

namespace :generate do

  desc 'Generate daily meetings for everyone'
  task :meetings => [:environment] do
    generate_meetings_for_users(4)
    spread_users_to_meetings(4)
    spread_users_without_meetings(4)
    delete_not_full_meetings_and_set_users_as_met(4)
    send_mails_to_every_user_with_meeting
  end
end

def users_without_meetings
  i = 0
  User.all.each do |u|
      if u.meetings.where(meeting_date: Date.today).count == 0
        i += 1
      end
  end
  #puts i
  return i
end

def spread_users_to_meetings(users_per_meeting)
  @found_meeting_flag = false
  if users_without_meetings >= users_per_meeting
    User.all.each do |u|
      @found_meeting_flag = false
      if u.meetings.where(meeting_date: Date.today).count == 0
        Meeting.where(meeting_date: Date.today).each do |m|
          if m.users.count  < users_per_meeting
            if m.users.all.count != 0
              m.users.all.each do |mu|
                if !u.has_met(mu) && !@found_meeting_flag
                  UserMeetingConnection.create(user_id: u.id,meeting_id: m.id, status: "pending")
                  #puts "UserAddedToMeeting " + u.id.to_s
                  @found_meeting_flag = true
                  break
                else
                  break
                end
              end
            else
              if u.meetings.where(meeting_date: Date.today).count == 0
                UserMeetingConnection.create(user_id: u.id,meeting_id: m.id, status: "pending")
                @found_meeting_flag = true
                # puts "UserAddedToMeeting through secound check" + u.id.to_s
                break
              end
            end
          end
        end
      end
    end
  end
end

def delete_not_full_meetings_and_set_users_as_met(users_per_meeting)
    Meeting.where(meeting_date: Date.today).each do |m|
      if m.users.count < users_per_meeting
        m.delete
        # puts "Meeting deleted"
      else
        m.add_users_to_met
      end
    end
end

def spread_users_without_meetings(users_per_meeting)
  User.all.each do |u|
    if u.meetings.where(meeting_date: Date.today).count == 0
      Meeting.where(meeting_date: Date.today).each  do |m|
        if m.users.count < users_per_meeting
          # puts "Spreaded others " + u.id.to_s
          UserMeetingConnection.create(user_id: u.id, meeting_id: m.id, status: "pending")
        end
      end
    end
  end
end

def generate_meetings_for_users(users_per_meeting)
  if users_without_meetings > users_per_meeting - 1
    while users_without_meetings/users_per_meeting > meetings_today - 1
      meeting = Meeting.create(name: "Generated meeting " + Date.today.to_s,meeting_date: Date.today, ammount_of_people: users_per_meeting)
      Chat.create(meeting_id: meeting.id)
      #puts "Meeting created"
    end
  else
    exit
  end
end

def send_mails_to_every_user_with_meeting
  users_with_meetings = UserMeetingConnection.where(created_at: Time.zone.now.beginning_of_day..Time.zone.now.end_of_day)
  users_with_meetings.each do |u|
    ConfirmationMailer.confirm_meeting(u.meeting_id,u.user_id).deliver_now
  end
end

def meetings_today
  return Meeting.where(meeting_date: Date.today).count
end
