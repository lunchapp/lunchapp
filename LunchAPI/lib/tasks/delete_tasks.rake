namespace :delete do

  desc 'delete_all_chats'
  task :chats => [:environment] do
    Chat.delete_all
  end

  desc 'delete_all_records_in_MetUser'
  task :met_users => [:environment] do
    MetUser.delete_all
  end
  
end
