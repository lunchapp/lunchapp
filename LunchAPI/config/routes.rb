Rails.application.routes.draw do

  resources :users, only: [:index, :create]
  #returns user with a certain uuid
  get '/users/:uuid' => 'users#show', as: 'user'
  #confirms a user account
  get '/users/confirm/:confirmation_token' => 'users#confirm'
  #returns the meetings of a user with given uuid
  get '/users/:uuid/meetings'  => 'users#get_user_meetings'
  #returns the chats of a user with given uuid
  get '/users/:uuid/chats' => 'users#get_user_chats'
  #returns the status of a user with given uuid in a meeting with a given uuid
  get '/users/:user_uuid/meetings/:meeting_uuid/status' => 'users#get_meeting_status'
  #returns all met users for a user with given uuid
  get '/users/:uuid/met_users' => 'users#get_met_users'
  #returns current user
  get '/user' => 'users#get_current'
  #returns all meetings of the current user
  get '/user/meetings' => 'users#get_current_user_meetings'
  #returns the company of  a user
  get '/users/:uuid/company' => 'users#get_user_ompany'
  #updates a user
  put '/users/:uuid' => 'users#update'
  #deletes a user
  delete 'users/:uuid' => 'users#destroy'

  put 'meetings/:uuid/vote/time/:vote' => 'meetings#vote_for_time'
  put 'meetings/:uuid/vote/location/:vote' => 'meetings#vote_for_location'
  get 'meetings/:uuid/vote/results' => 'meetings#get_vote_results'
  get 'meetings/:uuid/vote/location' => 'meetings#voted_for_location?'
  get 'meetings/:uuid/vote/time' => 'meetings#voted_for_time?'

  resources :companies, only: [:index, :create]
  #returns the company of current user
  get '/user/company' => 'users#get_current_user_company'
  #returns a company with given uuid
  get '/companies/:uuid' => 'companies#show'
  #returns all users in a company
  get '/companies/:uuid/users' => 'companies#company_users'
  #updates a company
  put '/companies/:uuid' => 'companies#update'
  #deletes a company
  delete '/companies/:uuid' => 'companies#destroy'
  #creates a company
  post '/companies' => 'companies#create'

  resources :meetings, only: [:index, :create]
  #returns meeting
  get '/meetings/:uuid' => 'meetings#show'
  #returns all users in a meeting
  get '/meetings/:uuid/users' => 'meetings#get_users_in_meeting'
  #updates a meeting
  put '/meetings/:uuid' => 'meetings#update'
  #accepts/declines meeting expects Accept or Decline as status
  get '/meetings/respond/:confirmation_token/:user_uuid/:status' => 'meetings#set_status'
  #deletes meeting
  delete '/meetings/:uuid' => 'meetings#destroy'

  post '/login' => 'sessions#create'
  delete '/logout' => 'sessions#destroy'

  #returns all messages in a chat in a meeting
  get '/meetings/:uuid/chat/messages' => 'chats#get_messages_of_meeting_chat'
  #returns the information about the chat of a meeting
  get '/meetings/:uuid/chat' => 'chats#get_meeting_chat'
  #creates a message
  post '/chat/:chat_id/message' => 'messages#create'
end
